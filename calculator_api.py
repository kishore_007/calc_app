"""Calculator Application """
from flask import Flask,render_template,request,url_for,redirect
#from flask_pymongo import PyMongo
from pymongo import ReturnDocument,MongoClient

app=Flask(__name__)
mongo_client_ref = MongoClient('database',27017)
#mongo_client_ref=PyMongo(app,uri="mongodb://localhost:27017/calc_db")
#application.config["MONGO_URI"] = 'mongodb://' + os.environ['MONGO_INITDB_ROOT_USERNAME'] + ':'
# + os.environ['MONGO_INITDB_ROOT_PASSWORD'] + '@'
# + os.environ['MONGODB_HOSTNAME'] + ':27017/'
# + os.environ['MONGO_INITDB_DATABASE']

db=mongo_client_ref.calc_db
if not db.calc_report.find_one({'operation':'+'}):
    db.calc_report.insert_one({'operation':'+',
    '+ operation done so far':0,
    'success':0,
    'failure':0})
if not db.calc_report.find_one({'operation':'*'}):
    db.calc_report.insert_one({'operation':'*',
    '* operation done so far':0,
    'success':0,
    'failure':0})
if not db.calc_report.find_one({'operation':'/'}):
    db.calc_report.insert_one({'operation':'/',
    '/ operation done so far':0,
    'success':0,
    'failure':0})
@app.route('/',methods=["GET"])
def index():
    """renders index template and take input from html page"""
    return render_template('index.html')

@app.route('/calculations',methods=['POST'])
def calculations():
    """takes input from form and send it to api"""
    input1= request.form['number1']
    input2=request.form['number2']
    operation=request.form['operation']
    try:
        input1=int(input1)
        input2=int(input2)
    except ValueError:
        if operation=='+':
            db.calc_report.find_one_and_update({'operation':operation},
            {'$inc':{'failure':1,'+ operation done so far':1}},
            return_document=ReturnDocument.AFTER)
        elif operation=='*':
            db.calc_report.find_one_and_update({'operation':operation},
            {'$inc':{'failure':1,'* operation done so far':1}},
            return_document=ReturnDocument.AFTER)
        else:
            db.calc_report.find_one_and_update({'operation':operation},
            {'$inc':{'failure':1,'/ operation done so far':1}},
            return_document=ReturnDocument.AFTER)
        calc_report_dict={'input1':input1,
        'input2':input2,
        'operation':operation,
        'result':"Value Error",
        'calculation_success':False
        }
        db.calculations.insert_one(calc_report_dict)
        return render_template('index.html',
        input1=input1,
        input2=input2,
        operation=operation,
        result="Input Contains Non-Numerical Values",
        calculation_success=False,
        error="operations can be done with numerics only")
    if operation == '+':
        call='add'
    elif operation == '/':
        call='div'
    else:
        call='mul'
    return redirect(url_for(call,number1=input1,number2=input2))


@app.route('/add/<number1>/<number2>',methods=['GET','POST'])
def add(number1,number2):
    """add and send resultant to index page and DB"""
    calc=int(number1)+int(number2)
    db.calc_report.find_one_and_update({'operation':'+'},{'$inc':{'success':1,
    '+ operation done so far':1}},
    return_document=ReturnDocument.AFTER)
    calc_report_dict={'input1':number1,
    'input2':number2,
    'operation':'+',
    'result':calc,
    'calculation_success':True}
    db.calculations.insert_one(calc_report_dict)
    return render_template('index.html',
    input1=number1,
    input2=number2,
    operation='+',
    result=calc,
    calculation_success=True)
@app.route('/div/<number1>/<number2>', methods=['GET','POST'])
def div(number1,number2):
    """divide and send resultant to index page and DB"""
    try:
        calc=int(number1)/int(number2)
    except ZeroDivisionError:
        db.calc_report.find_one_and_update({'operation':'/'},
        {'$inc':{'failure':1,'/ operation done so far':1}},
        return_document=ReturnDocument.AFTER)
        calc_report_dict={'input1':number1,
        'input2':number2,
        'operation':'/',
        'result':'ZeroDivision Error',
        'calculation_success':False}
        db.calculations.insert_one(calc_report_dict)
        return render_template('index.html',
        input1=number1,
        input2=number2,
        operation='/',
        result='Zero Division Error',
        calculation_success=False,
        error="Denominator must not be zero")
    db.calc_report.find_one_and_update({'operation': '/'}, {'$inc':
                                {'success': 1,'/ operation done so far' : 1}},
                                     return_document=ReturnDocument.AFTER)
    calc_report_dict={'input1':number1,
    'input2':number2,
    'operation':'/',
    'result':calc,
    'calculation_success':True}
    db.calculations.insert_one(calc_report_dict)
    return render_template('index.html',
                                    input1=number1,
                                    input2=number2,
                                    operation='/',
                                    result=calc,
                                    calculation_success=True)
@app.route('/mul/<number1>/<number2>', methods=['GET','POST'])
def mul(number1,number2):
    """multiply and send resultant to index page and DB"""
    calc=int(number1)*int(number2)
    db.calc_report.find_one_and_update({'operation': '*'},
     {'$inc': {'success': 1,'* operation done so far' : 1}},
     return_document=ReturnDocument.AFTER)
    calc_report_dict={'input1':number1,
    'input2':number2,
    'operation':'*',
    'result':calc,
    'calculation_success':True}
    db.calculations.insert_one(calc_report_dict)
    return render_template('index.html',
                                    input1=number1,
                                    input2=number2,
                                    operation='*',
                                    result=calc,
                                    calculation_success=True)

if __name__=='__main__':
    app.run(host='0.0.0.0',port='5000',debug=True)
    #app.run(debug=True)

