FROM python:3.8.10

RUN pip install virtualenv

ENV VIRTUAL_ENV = /venv

RUN virtualenv venv -p python

ENV PATH="VIRTUAL_ENV/bin:$PATH"

WORKDIR /calculator_flask

ADD . /calculator_flask

RUN pip install -r requirements.txt

COPY . /calculator_flask/

CMD ["python","calculator_api.py"]

