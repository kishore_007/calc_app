import requests
import pytest
url='http://127.0.0.1:5000'
#checking Index page response code
def test_index_page():
    index_response=requests.get(url+'/')
    assert index_response.status_code == 200
#checking api's response code
def test_api_pages():
    addition_test='/add/2/4'
    multiplication_test='/mul/5/8'
    division_test='/div/4/5'
    add_response=requests.get(url+addition_test)
    mul_response=requests.get(url+multiplication_test)
    div_response=requests.get(url+division_test)
    assert add_response.status_code == 200
    assert div_response.status_code ==  200
    assert mul_response.status_code == 200
#check bad requests
def test_bad_requests():
    bad_requests=requests.get(url+'/sub/3/7')
    assert bad_requests.status_code == 404

if(__name__=='__main__'):
    test_index_page()
    test_api_pages()
    test_bad_requests()

'''
#web scraping
from bs4 import BeautifulSoup
test_cases={
    'test_case1': '/add/2/4',
    'test_case2': '/mul/2/4',
    'test_case3': '/div/2/4',
    'test_case4': '/div/5/0',
    'test_case5': '/mul/a/5',
    'test_case6': '/add/3939/009n',
    'test_case7': '/add/0093899/4',

}
    for itr in test_cases.values():
        soup=BeautifulSoup(add_response.content,"html.parser")
        result_value=int(soup.find('h4').get_text()[-1])
        print(type(result_value))
        assert int(addition_test[-3])+int(addition_test[-1]) == result_value 
'''